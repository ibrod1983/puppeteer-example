const puppeteer = require('puppeteer');


(async () => {

    const browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null,
      args: ['--start-maximized'] 
    });
    const selector = '.inputSlider'//

    const page = await browser.newPage();
    await page.goto('http://erncalc.web2.moonsite.co.il/');
    await page.waitForSelector(selector);
    page.evaluate(()=>{
       alert('אוטומציה זו תקליד ערך 12345 במחיר הרכב, ואז תבדוק האם סכום ההחזר החודשי הוא 248') 
    }) 

    await page.focus(selector)
    await page.keyboard.press('Backspace')
    await page.keyboard.press('Backspace')
    await page.keyboard.press('Backspace')
    await page.keyboard.press('Backspace')
    await page.keyboard.press('Backspace')


    await page.keyboard.type('2345',{delay:1000})//

    const sumVal =  await page.$eval('.sumMonthlyRepaymentStyle', el => el.innerText);
    console.log(sumVal)
    if(sumVal == 248){
        await page.evaluate(() => alert('הכל כשורה. התקבל ערך 248 כצפוי'))
    }else{
        await page.evaluate(() => alert('התקבל ערך לא נכון'))
    }


})();
